# om-template

Import template([json file](https://gitee.com/opensourceway/om-template/tree/master/template)) in grafana. You could see [grafana import guide](https://grafana.com/docs/grafana/latest/reference/export_import/)


## HOW TO USE SCRIPT TO BAKUP
1. get grafana api key
```shell
grafana-> configuration-> api keys ->new key

```
2. excute script 
```shell
bash grafana_db_bak.sh $grafanahosts $apikey
```
